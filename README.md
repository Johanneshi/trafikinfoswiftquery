# TrafikinfoSwiftQuery

## Example

```swift

func queryForCenter(_ point: GeoPointExpressible, radius: Double) -> Data {
    
    AuthenticatedRequest(authenticationKey: "{AUTH}") {
        
        Query(objectType: "TrainStation", schemaVersion: "1") {
            
            Filter {
                Within(.center(point, radiusInMeter: radius), system: .sweref99tm)
            }
        }
    }
    .render()
}

func allStations(lastModifiedDate: Date? = nil) -> Data {
            
    AuthenticatedRequest(authenticationKey: "{AUTH}") {
        
        Query(objectType: "TrainStation", schemaVersion: "1") {
            
            Filter {
                
                EqualTo(true, forName: "Advertised")
                
                if let lastModifiedDate = lastModifiedDate {
                    GreaterThan(dateFormatter.string(from: lastModifiedDate), forName: "ModifiedTime")
                }
            }
        }
    }
    .render()
}
```

## Installation

### Swift Package Manager

dependencies: [
    .package(url: "https://Johanneshi@bitbucket.org/Johanneshi/trafikinfoswiftquery.git", .upToNextMajor(from: "1.0.0"))
]
