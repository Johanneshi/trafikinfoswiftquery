import XCTest

import TrafikinfoSwiftQueryTests

var tests = [XCTestCaseEntry]()
tests += TrafikinfoSwiftQueryTests.allTests()
XCTMain(tests)
