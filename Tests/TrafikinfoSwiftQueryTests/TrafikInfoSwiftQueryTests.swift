import XCTest

#if canImport(CoreLocation)
import CoreLocation
#endif

@testable import TrafikinfoSwiftQuery

final class TrafikInfoSwiftQueryTests: XCTestCase {
    
    func testOr() {
        XCTAssertEqual(Or { StringFragment("test"); StringFragment("test") }.render(), "<OR>testtest</OR>")
    }
    
    func testAnd() {
        XCTAssertEqual(And { StringFragment("test") }.render(), "<AND>test</AND>")
    }
    
    func testNot() {
        XCTAssertEqual(Not { StringFragment("test") }.render(), "<NOT>test</NOT>")
    }
    
    func testFilter() {
        XCTAssertEqual(Filter { StringFragment("test") }.render(), "<FILTER>test</FILTER>")
    }
    
    func testIn() {
        XCTAssertEqual(In(["1", "2", "3"], forName: "Values").render(),
                       "<IN name=\"Values\" value=\"1,2,3\" />")
    }
    
    func testInInts() {
        XCTAssertEqual(In([1, 2, 3], forName: "Values").render(),
                       "<IN name=\"Values\" value=\"1,2,3\" />")
    }
    
    func testInIntsVariadic() {
        XCTAssertEqual(In(1, 2, 3, forName: "Values").render(),
                       "<IN name=\"Values\" value=\"1,2,3\" />")
    }
    
    func testInVariadic() {
        XCTAssertEqual(In("1", "2", "3", forName: "Values").render(),
                       "<IN name=\"Values\" value=\"1,2,3\" />")
    }
    
    func testEqualTo() {
        XCTAssertEqual(EqualTo("test", forName: "property").render(),
                       "<EQ name=\"property\" value=\"test\" />")
    }
    
    func testNotEqualTo() {
        XCTAssertEqual(NotEqualTo("test", forName: "property").render(),
                       "<NE name=\"property\" value=\"test\" />")
    }
    
    func testGreaterThan() {
        XCTAssertEqual(GreaterThan("test", forName: "property").render(),
                       "<GT name=\"property\" value=\"test\" />")
    }
    
    func testGreaterThanOrEqualTo() {
        XCTAssertEqual(GreaterThanOrEqualTo("test", forName: "property").render(),
                       "<GTE name=\"property\" value=\"test\" />")
    }
    
    func testLessThan() {
        XCTAssertEqual(LessThan("test", forName: "property").render(),
                       "<LT name=\"property\" value=\"test\" />")
    }
    
    func testLessThanOrEqualTo() {
        XCTAssertEqual(LessThanOrEqualTo("test", forName: "property").render(),
                       "<LTE name=\"property\" value=\"test\" />")
    }
    
    func testElementMatch() {
        XCTAssertEqual(ElementMatch { StringFragment() }.render(),
                       "<ELEMENTMATCH></ELEMENTMATCH>")
    }
    
    func testExists() {
        XCTAssertEqual(Exists(true, forName: "property").render(),
                       "<EXISTS name=\"property\" value=\"true\" />")
        XCTAssertEqual(Exists(false, forName: "property").render(),
                       "<EXISTS name=\"property\" value=\"false\" />")
    }
    
    func testNotLike() {
        XCTAssertEqual(NotLike("test", forName: "property").render(),
                       "<NOTLIKE name=\"property\" value=\"test\" />")
    }
    
    func testLike() {
        XCTAssertEqual(Like("test", forName: "property").render(),
                       "<LIKE name=\"property\" value=\"test\" />")
    }
    
    func testGeoBoxAttributes() {
        
        let point1 = Geo.Point(latitude: 100, longitude: 40.3)
        let point2 = Geo.Point(latitude: 30, longitude: 40.4)

        
        let boxAttributes = Geo.Shape.box(lowerLeft: point1, upperRight: point2).attributes
        
        XCTAssertEqual(boxAttributes.render(), "shape=\"box\" value=\"40.3 100.0, 40.4 30.0\"")
    }
    
    func testGeoPolygonAttributes() {
        
        let point1 = Geo.Point(latitude: 100, longitude: 40.3)
        let point2 = Geo.Point(latitude: 30, longitude: 40.4)
        
        let polygonAttributes = Geo.Shape.polygon(Array(repeating: point1, count: 2) + Array(repeating: point2, count: 2))
            .attributes
        
        XCTAssertEqual(polygonAttributes.render(),
                       "shape=\"polygon\" value=\"40.3 100.0, 40.3 100.0, 40.4 30.0, 40.4 30.0\"")
    }
    
    func testCenterAttributes() {
        
        let point1 = Geo.Point(latitude: 100, longitude: 40.3)
        
        let centerAttributes = Geo.Shape.center(point1, radiusInMeter: 200).attributes
        
        XCTAssertEqual(centerAttributes.render(),
                       "radius=\"200.0m\" shape=\"center\" value=\"40.3 100.0\"")
    }
    
    func testDistanceAttributes() {
        
        let point1 = Geo.Point(latitude: 100, longitude: 40.3)

        let distanceAttributes = Geo.Distance(point: point1, maxDistance: 100, minDistance: 10).attributes
        
        XCTAssertEqual(distanceAttributes.render(),
                       "maxdistance=\"100.0\" mindistance=\"10.0\" value=\"40.3 100.0\"")
    }
    
    func testWithin() {
        
        let point1 = Geo.Point(latitude: 100, longitude: 40.3)
        let point2 = Geo.Point(latitude: 30, longitude: 40.4)
        
        XCTAssertEqual(Within(.box(lowerLeft: point1, upperRight: point2), system: .sweref99tm).render(), "<WITHIN name=\"Geometry.SWEREF99TM\" shape=\"box\" value=\"40.3 100.0, 40.4 30.0\" />")
    }
    
        func testWithinWithNamePrefix() {
                        
            let element = Within(
                .center(Geo.Point(latitude: 6398983, longitude: 320011), radiusInMeter: 1000), name: .prefixed(prefix: "Deviation", .sweref99tm))
            
            let expectedString = """
    <WITHIN name="Deviation.Geometry.SWEREF99TM" radius="1000.0m" shape="center" value="320011.0 6398983.0" />
    """
            
            XCTAssertEqual(element.render(), expectedString)
        }
    
    func testIntersects() {
        
        let point1 = Geo.Point(latitude: 100, longitude: 40.3)
        let point2 = Geo.Point(latitude: 30, longitude: 40.4)
        
        XCTAssertEqual(Intersects(.polygon([point1, point2]), system: .wgs84).render(),
                       "<INTERSECTS name=\"Geometry.WGS84\" shape=\"polygon\" value=\"40.3 100.0, 40.4 30.0\" />")
    }
    
    func testAttributes() {
        
        let attributes1: Attributes = [.value: "1", .shape: "2", .radius: "3"]
        let attributes2: Attributes = [.value: "2", .shape: "3", .radius: "4"]
        
        let merge1 = attributes1.merging(attributes2)
                
        XCTAssertEqual(merge1.render(), "radius=\"4\" shape=\"3\" value=\"2\"")
        
        let merge2 = merge1.merging([.schemaVersion: "1"])
        
        XCTAssertEqual(merge2.render(), "radius=\"4\" schemaversion=\"1\" shape=\"3\" value=\"2\"")
        
        let merge3 = merge2.merging(.init(["customattribute": "5"]))
        
        XCTAssertEqual(merge3.render(),
                       "customattribute=\"5\" radius=\"4\" schemaversion=\"1\" shape=\"3\" value=\"2\"")
    }
    
    func testQuery() {
        
        let query = Query(
            objectType: "TrainStation",
            schemaVersion: "1",
            includeDeletedObjects: true,
            limit: 100,
            skip: 10,
            lastModified: true,
            additionalAttributes: ["test": "test"]) {
                StringFragment()
        }
        
        let expectedString = """
<QUERY includedeletedobjects=\"true\" lastmodified=\"true\" limit=\"100\" objecttype=\"TrainStation\" schemaversion=\"1\" skip=\"10\" test=\"test\"></QUERY>
"""
        XCTAssertEqual(query.render(), expectedString)
    }
    
    func testLogin() {
        let key = UUID().uuidString
        XCTAssertEqual(Login(authenticationKey: key).render(), "<LOGIN authenticationkey=\"\(key)\" />")
    }
    
    func testRequest() {
        XCTAssertEqual(Request { StringFragment() }.render(), "<REQUEST></REQUEST>")
    }
    
    func testNotIn() {
        XCTAssertEqual(NotIn(["1", "2", "3"], forName: "Values").render(),
                       "<NOTIN name=\"Values\" value=\"1,2,3\" />")
    }
    
    func testNotInVariadic() {
        XCTAssertEqual(NotIn("1", "2", "3", forName: "Values").render(),
                       "<NOTIN name=\"Values\" value=\"1,2,3\" />")
    }
    
    func testNotInInts() {
        XCTAssertEqual(NotIn([1, 2, 3], forName: "Values").render(),
                       "<NOTIN name=\"Values\" value=\"1,2,3\" />")
    }
    
    func testNotInIntsVariadic() {
        XCTAssertEqual(NotIn(1, 2, 3, forName: "Values").render(),
                       "<NOTIN name=\"Values\" value=\"1,2,3\" />")
    }
    
    func testForEach() {
                        
        let foreach = ForEach(0...3) {
            
            if $0 != 1 {
                StringFragment(String($0))
            }
            
            if $0 % 2 == 0 {
                StringFragment(String("_"))
            } else {
                StringFragment(String("*"))
            }
        }

        XCTAssertEqual(foreach.render(), "0_*2_3*")
    }
    
    func testForEachVariadic() {
                        
        let foreach = ForEach(0, 1, 2, 3) {
            
            if $0 != 1 {
                StringFragment(String($0))
            }
            
            if $0 % 2 == 0 {
                StringFragment(String("_"))
            } else {
                StringFragment(String("*"))
            }
        }
        
        XCTAssertEqual(foreach.render(), "0_*2_3*")
    }
    
    func testDistinct() {
        XCTAssertEqual(Distinct("Test").render(), "<DISTINCT>Test</DISTINCT>")
    }
    
    func testExclude() {
        XCTAssertEqual(Exclude("Test").render(), "<EXCLUDE>Test</EXCLUDE>")
    }
    
    func testInclude() {
        XCTAssertEqual(Include("Test").render(), "<INCLUDE>Test</INCLUDE>")
    }
    
    func testNear() {
        let point = Geo.Point(latitude: 56.6, longitude: 12.8)
        XCTAssertEqual(Near(point, maxDistance: 20, minDistance: 10, system: .wgs84).render(),
                       "<NEAR maxdistance=\"20.0\" mindistance=\"10.0\" name=\"Geometry.WGS84\" value=\"12.8 56.6\" />")
    }
    
    func testNearCoreLocation() {
        
        #if canImport(CoreLocation)

        let location = CLLocation(latitude: 56.6, longitude: 12.8)
        XCTAssertEqual(Near(location, maxDistance: 20, minDistance: 10, system: .wgs84).render(),
                       "<NEAR maxdistance=\"20.0\" mindistance=\"10.0\" name=\"Geometry.WGS84\" value=\"12.8 56.6\" />")
        #endif
    }
    
    
    func testCustomElement() {
        
        let element = Element(name: "TestElement", attributes: ["property1": "test", "property2": "test"])
        
        XCTAssertEqual(element.render(), "<TestElement property1=\"test\" property2=\"test\" />")
    }
    
    func testCustomElementWithChildren() {
        
        let element = Element(name: "TestElement", attributes: ["property1": "test", "property2": "test"]) {
            StringFragment("Test")
        }
                
        XCTAssertEqual(element.render(), "<TestElement property1=\"test\" property2=\"test\">Test</TestElement>")
    }
    
    func testAuthenticatedRequest() {
        
        let element = AuthenticatedRequest(authenticationKey: "authenticationKey") {
            StringFragment("Test")
        }
        
        XCTAssertEqual(element.render(), "<REQUEST><LOGIN authenticationkey=\"authenticationKey\" />Test</REQUEST>")
    }
    
    func testRenderData() {
        
        let element = AuthenticatedRequest(authenticationKey: "authenticationKey") {
            StringFragment("Test")
        }
        
        let expectedResult = "<REQUEST><LOGIN authenticationkey=\"authenticationKey\" />Test</REQUEST>"
        
        XCTAssertEqual(String(data: element.render(), encoding: .utf8), expectedResult)
    }
    
    func testUrlRequest() {
                
        let query = AuthenticatedRequest(authenticationKey: "authenticationKey") {
            
            Query(objectType: "TrainStation", schemaVersion: "1") {
                
                ForEach(0...3) {
                    Include($0)
                }
                
                Filter {
                    EqualTo(true, forName: "Advertised")
                }
            }
        }
        
        let request = URLRequest(version: "1.3", format: "json") { query }
        
        XCTAssertEqual(request.httpBody, query.render())
        XCTAssertEqual(request.httpMethod, "POST")
        XCTAssertEqual(request.url, URL(string: "https://api.trafikinfo.trafikverket.se/1.3/data.json")!)
    }
    
    func testEvalFunctionWithPrefix() {
        
        let element = Eval(alias: "MyFirstFunctionResult", function: "$function.SomeLib_v1.SomeMethod(1238, FF)")
        
        XCTAssertEqual(element.render(), "<EVAL alias=\"MyFirstFunctionResult\" function=\"$function.SomeLib_v1.SomeMethod(1238, FF)\" />")
    }
    
    func testEvalFunctionWithoutPrefix() {
        
        let element = Eval(alias: "MyFirstFunctionResult", function: "SomeLib_v1.SomeMethod(1238, FF)")
        
        XCTAssertEqual(element.render(), "<EVAL alias=\"MyFirstFunctionResult\" function=\"$function.SomeLib_v1.SomeMethod(1238, FF)\" />")
    }
    
    static var allTests = [
        ("testOr", testOr),
        ("testAnd", testAnd),
        ("testNot", testNot),
        ("testFilter", testFilter),
        ("testIn", testIn),
        ("testInVariadic", testInVariadic),
        ("testEqualTo", testEqualTo),
        ("testNotEqualTo", testNotEqualTo),
        ("testGreaterThan", testGreaterThan),
        ("testGreaterThanOrEqualTo", testGreaterThanOrEqualTo),
        ("testLessThan", testLessThan),
        ("testLessThanOrEqualTo", testLessThanOrEqualTo),
        ("testElementMatch", testElementMatch),
        ("testExists", testExists),
        ("testNotLike", testNotLike),
        ("testLike", testLike),
        ("testGeoBoxAttributes", testGeoBoxAttributes),
        ("testGeoPolygonAttributes", testGeoPolygonAttributes),
        ("testCenterAttributes", testCenterAttributes),
        ("testDistanceAttributes", testDistanceAttributes),
        ("testWithin", testWithin),
        ("testIntersects", testIntersects),
        ("testAttributes", testAttributes),
        ("testQuery", testQuery),
        ("testLogin", testLogin),
        ("testRequest", testRequest),
        ("testNotIn", testNotIn),
        ("testForEach", testForEach),
        ("testExclude", testExclude),
        ("testInclude", testInclude),
        ("testNear", testNear),
        ("testCustomElement", testCustomElement),
        ("testCustomElementWithChildren", testCustomElementWithChildren),
        ("testAuthenticatedRequest", testAuthenticatedRequest),
        ("testRenderData", testRenderData),
        ("testUrlRequest", testUrlRequest),
        ("testEvalFunctionWithPrefix", testEvalFunctionWithPrefix),
        ("testEvalFunctionWithoutPrefix", testEvalFunctionWithoutPrefix),
        ("testNearCoreLocation", testNearCoreLocation)
    ]
}
