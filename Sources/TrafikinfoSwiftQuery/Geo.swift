//
//  Geo.swift
//  
//
//  Created by Johannes Hintze on 2020-03-10.
//

import Foundation

#if canImport(CoreLocation)
import CoreLocation

extension CLLocationCoordinate2D: GeoPointExpressible {}

extension CLLocation: GeoPointExpressible {
    public var latitude: Double {
        coordinate.latitude
    }
    
    public var longitude: Double {
        coordinate.longitude
    }
}

#endif

public struct Geo {
    
    public struct Point: GeoPointExpressible {
        public let latitude: Double
        public let longitude: Double
        
        init(latitude: Double, longitude: Double) {
            self.latitude = latitude
            self.longitude = longitude
        }
    }
    
    public enum Shape {
        case polygon([GeoPointExpressible])
        case box(lowerLeft: GeoPointExpressible, upperRight: GeoPointExpressible)
        case center(GeoPointExpressible, radiusInMeter: Double)
        
        var attributes: Attributes {
            
            switch self {
            case let .box(lowerLeft, upperRight):
                return [.shape: "box", .value: [lowerLeft, upperRight].asString()]
            case let .polygon(points):
                return [.shape: "polygon", .value: points.asString()]
            case let .center(point, radiusInMeter):
                return [.shape: "center", .radius: "\(radiusInMeter)m", .value: point.asString()]
            }
        }
    }
    
    public struct Distance {
        let point: GeoPointExpressible
        let maxDistance: Double
        let minDistance: Double
        
        init(point: GeoPointExpressible, maxDistance: Double = 0, minDistance: Double = 0) {
            self.point = point
            self.maxDistance = maxDistance
            self.minDistance = minDistance
        }
        
        var attributes: Attributes {
            [.value: point.asString(),
             .minDistance: String(minDistance),
             .maxDistance: String(maxDistance)]
        }
    }
    
    public enum System: String {
        case wgs84 = "Geometry.WGS84"
        case sweref99tm = "Geometry.SWEREF99TM"
    }
    
    public enum Kind {
        case distance(Distance)
        case shape(Shape)
        
        var attributes: Attributes {
            switch self {
            case let .distance(distance): return distance.attributes
            case let .shape(shape): return shape.attributes
            }
        }
    }
    
    public enum Name {
        case prefixed(prefix: String, System)
        case system(System)
        
        var attributes: Attributes {
            
            switch self {
            case let .prefixed(prefix, system):
                return [.name: "\(prefix).\(system.rawValue)"]
            case let .system(system):
                return [.name: system.rawValue]
            }
        }
    }
    
    public let name: Name
    public let kind: Kind
    
    var attributes: Attributes {
        name.attributes.merging(kind.attributes)
    }
}

public protocol GeoPointExpressible {
    
    var latitude: Double { get }
    var longitude: Double { get }
    
    func asString() -> String
}

extension GeoPointExpressible {
    
    public func asString() -> String {
        "\(longitude) \(latitude)"
    }
}

extension Array where Element == GeoPointExpressible {
    
    func asString() -> String {
        map { $0.asString() }.joined(separator: ", ")
    }
}
