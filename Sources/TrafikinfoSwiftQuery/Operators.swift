//
//  Operators.swift
//  
//
//  Created by Johannes Hintze on 2020-03-08.
//

import Foundation

public struct And: OperatorElement {
    
    let xml: XMLRendering
    
    public init(@XMLBuilder _ children: () -> XMLRendering) {
        xml = Element(name: .and, children: children)
    }
}

public struct AuthenticatedRequest: OperatorElement {
    
    let xml: XMLRendering
    
    public init(authenticationKey: String, _ children: () -> XMLRendering) {
        
        xml = Request {
            Login(authenticationKey: authenticationKey)
            children()
        }
    }
}

public struct Distinct: OperatorElement {
    
    let xml: XMLRendering
    
    public init(_ value: CustomStringConvertible) {
        xml = Element(name: .distinct) { StringFragment(value) }
    }
}

public struct ElementMatch: OperatorElement {
    
    let xml: XMLRendering
    
    public init(@XMLBuilder _ children: () -> XMLRendering) {
        xml = Element(name: .elementMatch, children: children)
    }
}

public struct Eval: OperatorElement {
    
    let xml: XMLRendering
    
    public init(alias: String, function: String) {
        
        let functionPrefix = "$function"
        
        let finalFunction: String
        
        if !function.hasPrefix(functionPrefix) {
            finalFunction = "\(functionPrefix).\(function)"
        } else {
            finalFunction = function
        }
        
        xml = Element(name: .eval, attributes: [.alias: alias, .function: finalFunction])
    }
}

public struct EqualTo: OperatorElement {
    
    let xml: XMLRendering
    
    public init(_ value: CustomStringConvertible, forName name: String) {
        xml = Element(name: .eq, attributes: [.name: name, .value: "\(value)"])
    }
}

public struct Exclude: OperatorElement {
    
    let xml: XMLRendering
    
    public init(_ value: CustomStringConvertible) {
        xml = Element(name: .exclude) { StringFragment(value) }
    }
}

public struct Exists: OperatorElement {
    
    let xml: XMLRendering
    
    public init(_ value: Bool, forName name: String) {
        xml = Element(name: .exists, attributes: [.name: name, .value: "\(value)"])
    }
}

public struct Filter: OperatorElement {
    
    let xml: XMLRendering
    
    public init(@XMLBuilder _ children: () -> XMLRendering) {
        xml = Element(name: .filter, children: children)
    }
}

public struct ForEach: OperatorElement {
    
    let xml: XMLRendering
    
    public init<Value>(_ values: Value..., @XMLBuilder children: (Value) -> XMLRendering) {
        self.init(values, children)
    }
    
    public init<Values: Collection>(_ values: Values, @XMLBuilder _ children: (Values.Element) -> XMLRendering) {
        xml = values.map { children($0) }
    }
}

public struct GreaterThan: OperatorElement {
    
    let xml: XMLRendering
    
    public init(_ value: CustomStringConvertible, forName name: String) {
        xml = Element(name: .gt, attributes: [.name: name, .value: "\(value)"])
    }
}

public struct GreaterThanOrEqualTo: OperatorElement {
    
    let xml: XMLRendering
    
    public init(_ value: CustomStringConvertible, forName name: String) {
        xml = Element(name: .gte, attributes: [.name: name, .value: "\(value)"])
    }
}

public struct In: OperatorElement {
    
    let xml: XMLRendering
    
    public init(_ values: [CustomStringConvertible], forName name: String) {
        
        if let strings = values as? [String] {
            self.init(strings, forName: name)
        } else {
            self.init(values.map { "\($0)" }, forName: name)
        }
    }
    
    public init(_ values: CustomStringConvertible..., forName name: String) {
        self.init(values, forName: name)
    }
    
    private init(_ values: [String], forName name: String) {
        
        xml = Element(
            name: .in,
            attributes: [.name: name, .value: values.joined(separator: ",")])
    }
}

public struct Include: OperatorElement {
    
    let xml: XMLRendering
    
    public init(_ value: CustomStringConvertible) {
        xml = Element(name: .include) { StringFragment(value) }
    }
}

public struct Intersects: OperatorElement {
    
    let xml: XMLRendering
    
    public init(_ shape: Geo.Shape, system: Geo.System) {
        self.init(shape, name: .system(system))
    }
    
    public init(_ shape: Geo.Shape, name: Geo.Name) {
        let geo = Geo(name: name, kind: .shape(shape))
        xml = Element(
            name: .intersects,
            attributes: geo.attributes)
    }
}

public struct LessThan: OperatorElement {
    
    let xml: XMLRendering
    
    public init(_ value: CustomStringConvertible, forName name: String) {
        xml = Element(name: .lt, attributes: [.name: name, .value: "\(value)"])
    }
}

public struct LessThanOrEqualTo: OperatorElement {
    
    let xml: XMLRendering
    
    public init(_ value: CustomStringConvertible, forName name: String) {
        xml = Element(name: .lte, attributes: [.name: name, .value: "\(value)"])
    }
}

public struct Like: OperatorElement {
    
    let xml: XMLRendering
    
    public init(_ value: CustomStringConvertible, forName name: String) {
        xml = Element(name: .like, attributes: [.name: name, .value: "\(value)"])
    }
}

public struct Login: OperatorElement {
    
    let xml: XMLRendering
    
    public init(authenticationKey: String) {
        xml = Element(name: .login, attributes: [.authenticationKey: authenticationKey])
    }
}

public struct Near: OperatorElement {
    
    let xml: XMLRendering
    
    public init(
        _ point: GeoPointExpressible,
        maxDistance: Double = 0,
        minDistance: Double = 0,
        system: Geo.System) {
        
        self.init(point, maxDistance: maxDistance, minDistance: minDistance, name: .system(system))
    }
    
    public init(
        _ point: GeoPointExpressible,
        maxDistance: Double = 0,
        minDistance: Double = 0,
        name: Geo.Name) {
        
        let distance = Geo.Distance(
            point: point,
            maxDistance: maxDistance,
            minDistance: minDistance)
        
        let geo = Geo(name: name, kind: .distance(distance))
        
        xml = Element(name: .near, attributes: geo.attributes)
    }
}

public struct Not: OperatorElement {
    
    let xml: XMLRendering
    
    public init(@XMLBuilder _ children: () -> XMLRendering) {
        xml = Element(name: .not, children: children)
    }
}

public struct NotEqualTo: OperatorElement {
    
    let xml: XMLRendering
    
    public init(_ value: CustomStringConvertible, forName name: String) {
        xml = Element(name: .ne, attributes: [.name: name, .value: "\(value)"])
    }
}

public struct NotIn: OperatorElement {
    
    let xml: XMLRendering
    
    public init(_ values: [CustomStringConvertible], forName name: String) {
        
        if let strings = values as? [String] {
            self.init(strings, forName: name)
        } else {
            self.init(values.map { "\($0)" }, forName: name)
        }
    }
    
    public init(_ values: CustomStringConvertible..., forName name: String) {
        self.init(values, forName: name)
    }
    
    private init(_ values: [String], forName name: String) {
        
        xml = Element(
            name: .notIn,
            attributes: [.name: name, .value: values.joined(separator: ",")])
    }
}

public struct NotLike: OperatorElement {
    
    let xml: XMLRendering
    
    public init(_ value: CustomStringConvertible, forName name: String) {
        xml = Element(name: .notLike, attributes: [.name: name, .value: "\(value)"])
    }
}

public struct Or: OperatorElement {
    
    let xml: XMLRendering
    
    public init(@XMLBuilder _ children: () -> XMLRendering) {
        xml = Element(name: .or, children: children)
    }
}

public struct Query: OperatorElement {
    
    let xml: XMLRendering
    
    public init(objectType: ObjectType,
                schemaVersion: String,
                id: String? = nil,
                includeDeletedObjects: Bool? = nil,
                limit: Int? = nil,
                orderBy: String? = nil,
                skip: Int? = nil,
                lastModified: Bool? = nil,
                changeId: String? = nil,
                sseUrl: Bool? = nil,
                additionalAttributes: [String: String]? = nil,
                @XMLBuilder _ children: () -> XMLRendering) {
                
        self.init(objectType: objectType.rawValue,
                  schemaVersion: schemaVersion,
                  id: id,
                  includeDeletedObjects: includeDeletedObjects,
                  limit: limit,
                  orderBy: orderBy,
                  skip: skip,
                  lastModified: lastModified,
                  changeId: changeId,
                  sseUrl: sseUrl,
                  additionalAttributes: additionalAttributes,
                  children)
    }
    
    public init(objectType: String,
                schemaVersion: String,
                id: String? = nil,
                includeDeletedObjects: Bool? = nil,
                limit: Int? = nil,
                orderBy: String? = nil,
                skip: Int? = nil,
                lastModified: Bool? = nil,
                changeId: String? = nil,
                sseUrl: Bool? = nil,
                additionalAttributes: [String: String]? = nil,
                @XMLBuilder _ children: () -> XMLRendering) {
        
        let attributesDictionary: [Attributes.Name: String] = [
            .objectType: objectType,
            .schemaVersion: schemaVersion,
            .includeDeletedObjects: includeDeletedObjects.map { String($0) },
            .limit: limit.map { String($0) },
            .orderBy: orderBy,
            .skip: skip.map { String($0) },
            .lastModified: lastModified.map { String($0) },
            .changeId: changeId,
            .sseUrl: sseUrl.map { String($0) }
            ]
            .compactMapValues { $0 }
        
        let attributes = additionalAttributes
            .map {
                Attributes(attributesDictionary).merging($0)
            } ?? Attributes(attributesDictionary)
        
        xml = Element(
            name: .query,
            attributes: attributes,
            children: children)
    }
}

public struct Request: OperatorElement {
    
    let xml: XMLRendering
    
    public init(@XMLBuilder _ children: () -> XMLRendering) {
        xml = Element(name: .request, children: children)
    }
}

public struct Within: OperatorElement {
    
    let xml: XMLRendering
    
    public init(_ shape: Geo.Shape, system: Geo.System) {
        self.init(shape, name: .system(system))
    }
    
    public init(_ shape: Geo.Shape, name: Geo.Name) {
        let geo = Geo(name: name, kind: .shape(shape))
        xml = Element(name: .within, attributes: geo.attributes)
    }
}

fileprivate protocol OperatorElement: XMLRendering {
    var xml: XMLRendering { get }
}

extension OperatorElement {
    
    public func render() -> String {
        xml.render()
    }
}
