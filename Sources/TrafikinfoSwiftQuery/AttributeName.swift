//
//  AttributeName.swift
//  
//
//  Created by Johannes Hintze on 2020-03-08.
//

import Foundation

extension Attributes {
    
    enum Name: String, CaseIterable {
        case authenticationKey = "authenticationkey"
        case name
        case value
        case shape
        case radius
        case minDistance = "mindistance"
        case maxDistance = "maxdistance"
        
        case objectType = "objecttype"
        case schemaVersion = "schemaversion"
        case id
        case includeDeletedObjects = "includedeletedobjects"
        case limit
        case orderBy = "orderby"
        case skip
        case lastModified = "lastmodified"
        case changeId = "changeid"
        case alias
        case function
        case sseUrl = "sseurl"
    }
}
