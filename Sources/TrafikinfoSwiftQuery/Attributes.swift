//
//  Attributes.swift
//  
//
//  Created by Johannes Hintze on 2020-03-10.
//

import Foundation

struct Attributes: Hashable {
    
    private let dictionary: [String: String]
    
    init(_ attributes: [Name: String]) {
        self.dictionary = attributes.reduce(into: [:]) { $0[$1.key.rawValue] = $1.value }
    }
    
    init(_ attributes: [String: String]) {
        self.dictionary = attributes
    }
    
    func merging(_ other: [String: String]) -> Attributes {
        .init(dictionary.merging(other) { _, new in new })
    }
    
    func merging(_ other: [Attributes.Name: String]) -> Attributes {
        .init(dictionary.merging(Attributes(other).dictionary) { _, new in new })
    }
    
    func merging(_ other: Attributes) -> Attributes {
        .init(dictionary.merging(other.dictionary) { _, new in new })
    }
}

extension Attributes: ExpressibleByDictionaryLiteral {
    
    init(dictionaryLiteral elements: (Name, String)...) {
        self.init(elements.reduce(into: [:]) { $0[$1.0.rawValue] = $1.1 })
    }
}

extension Attributes: XMLRendering {
    
    func render() -> String {
        dictionary.map { "\($0.key)=\"\($0.value)\"" }.sorted().joined(separator: " ")
    }
}
