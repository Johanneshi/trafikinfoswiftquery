//
//  XMLBuilder.swift
//  
//
//  Created by Johannes Hintze on 2020-03-08.
//

import Foundation

@resultBuilder public struct XMLBuilder {

    public static func buildBlock(_ items: XMLRendering...) -> XMLRendering {
        items
    }
    
    public static func buildEither(first: XMLRendering) -> XMLRendering {
        first
    }
    
    public static func buildEither(second: XMLRendering) -> XMLRendering {
        second
    }
    
    public static func buildIf(_ segment: XMLRendering?) -> XMLRendering {
      segment ?? StringFragment()
    }
    
    public static func buildArray(_ array: [XMLRendering]) -> XMLRendering {
        array
    }
}
