//
//  Fragment.swift
//  
//
//  Created by Johannes Hintze on 2020-03-08.
//

import Foundation

public struct StringFragment {
    
    let text: String
    
    init(_ text: String) {
        self.text = text
    }
}

extension StringFragment: CustomStringConvertible {
    
    public var description: String {
        text
    }
}

extension StringFragment: XMLRendering {
    
    public func render() -> String {
        String(describing: text)
    }
}

extension StringFragment: ExpressibleByStringLiteral {
    
    public init(stringLiteral value: StaticString) {
        self.init(String(describing: value))
    }
}

public extension StringFragment {
    
    init() {
        self = ""
    }
    
    init(_ text: CustomStringConvertible) {
        self.init("\(text)")
    }
}
