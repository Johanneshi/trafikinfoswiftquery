//
//  XMLRendering.swift
//  
//
//  Created by Johannes Hintze on 2020-03-08.
//

import Foundation

public protocol XMLRendering {
    func render() -> String
}

public extension XMLRendering {
    
    func render() -> Data {
        render().data(using: .utf8)!
    }
}

extension Array: XMLRendering where Element == XMLRendering {
    
    public func render() -> String {
        reduce(into: String()) { $0 += $1.render() }
    }
}
