//
//  File.swift
//  
//
//  Created by Johannes Hintze on 2020-04-01.
//

import Foundation

public extension Query {
    
    enum ObjectType: String {
        case railCrossing = "RailCrossing"
        case reasonCode = "ReasonCode"
        case trainAnnouncement = "TrainAnnouncement"
        case trainMessage = "TrainMessage"
        case trainStation = "TrainStation"
        
        case camera = "Camera"
        case ferryAnnouncement = "FerryAnnouncement"
        case ferryRoute = "FerryRoute"
        case icon = "Icon"
        case parking = "Parking"
        case roadCondition = "RoadCondition"
        case roadConditionOverview = "RoadConditionOverview"
        case situation = "Situation"
        case trafficFlow = "TrafficFlow"
        case trafficSafetyCamera = "TrafficSafetyCamera"
        case travelTimeRoute = "TravelTimeRoute"
        case weatherStation = "WeatherStation"
        
        case measurementData100 = "MeasurementData100"
        case measurementData20 = "MeasurementData20"
        case pavementData = "PavementData"
        case roadData = "RoadData"
        case roadGeometry = "RoadGeometry"
    }
}
