//
//  ElementName.swift
//  
//
//  Created by Johannes Hintze on 2020-03-08.
//

import Foundation

extension Element {
    
    struct Name {
        
        let rawValue: String
        
        init(_ rawValue: String) {
            self.rawValue = rawValue
        }
    }
}

extension Element.Name {
    
    static let eq = Self("EQ")
    static let ne = Self("NE")
    
    static let gt = Self("GT")
    static let gte = Self("GTE")
    
    static let lt = Self("LT")
    static let lte = Self("LTE")
    
    static let like = Self("LIKE")
    static let exists = Self("EXISTS")
    static let notLike = Self("NOTLIKE")
    static let `in` = Self("IN")
    static let notIn = Self("NOTIN")
    
    static let within = Self("WITHIN")
    static let intersects = Self("INTERSECTS")
    static let near = Self("NEAR")
    
    static let or = Self("OR")
    static let and = Self("AND")
    static let elementMatch = Self("ELEMENTMATCH")
    static let not = Self("NOT")

    static let include = Self("INCLUDE")
    static let exclude = Self("EXCLUDE")
    static let distinct = Self("DISTINCT")
    
    static let request = Self("REQUEST")
    static let query = Self("QUERY")
    static let filter = Self("FILTER")
    static let login = Self("LOGIN")
    
    static let eval = Self("EVAL")
}
