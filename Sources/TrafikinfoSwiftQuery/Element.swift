//
//  Element.swift
//  
//
//  Created by Johannes Hintze on 2020-03-08.
//

import Foundation

public struct Element: XMLRendering {
    
    private let name: String
    private let attributes: XMLRendering?
    
    private let children: XMLRendering?
    
    public init(
        name: String,
        attributes: [String: String]? = nil,
        @XMLBuilder children: () -> XMLRendering) {
        self.init(
            name: name,
            attributes: attributes.map(Attributes.init),
            children: children())
    }
    
    public init(
        name: String,
        attributes: [String: String]? = nil) {
        self.init(
            name: name,
            attributes: attributes.map(Attributes.init),
            children: nil)
    }
    
    init(
        name: Name,
        attributes: Attributes? = nil,
        @XMLBuilder children: () -> XMLRendering) {
        self.init(name: name.rawValue, attributes: attributes, children: children())
    }
    
    init(
        name: Name,
        attributes: Attributes? = nil) {
        self.init(name: name.rawValue, attributes: attributes, children: nil)
    }
    
    private init(name: String, attributes: XMLRendering?, children: XMLRendering?) {
        self.name = name
        self.attributes = attributes
        self.children = children
    }
    
    public func render() -> String {
        
        let tagEnd = children == nil ? "/" : nil
        
        var result = """
        <\([name, attributes?.render(), tagEnd].compactMap { $0 }.joined(separator: " "))>
        """
        
        guard let children = children else { return result }
        
        result += children.render()
        result += "</\(name)>"
        
        return result
    }
}
