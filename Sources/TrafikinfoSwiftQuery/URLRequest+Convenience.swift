//
//  URLRequest+Convenience.swift
//  
//
//  Created by Johannes Hintze on 2020-03-24.
//

import Foundation

public extension URLRequest {
    
    init(version: String, format: String, @XMLBuilder _ query: () -> XMLRendering) {
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "api.trafikinfo.trafikverket.se"
        urlComponents.path = "/\(version)/data.\(format)"
        
        guard let url = urlComponents.url else { fatalError() }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpBody = query().render()
        urlRequest.httpMethod = "POST"
        
        let charset = CFStringConvertEncodingToIANACharSetName(
                    CFStringConvertNSStringEncodingToEncoding(String.Encoding.utf8.rawValue))
        
        urlRequest.setValue(
            "text/xml; charset=\(String(describing: charset))",
            forHTTPHeaderField: "Content-Type")

        self = urlRequest
    }
}
